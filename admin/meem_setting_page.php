<?php

 /* Intro: This file is used to set settings page layout. 
 *  Version: See 'function.php' file
 *  Date :24-08-2015
 *  New Features: 
 *  SVG image support added in header, a UI has been implemented to get the image from Media library
 *  Plugin for my Child theme
 *  Theme Features:
 *   -Admin settings page
 *   -Multicolumn post thumbnail support.
 *   -upto 4 columns
 * 	 -RWD based design
 *   -Specialy designed colorful themes 
 *   -Slectable via admin panel
 *   -Color Theme selection made easy by using JQ based colorstrip visible while selecting option in Admin panel.
 *   -Categories based Post selection
 *   - SVG image support through a svg plugin by benbodhi 
 *   - I made this responsive by making addition in plugin file svg-inline.js
 *   - Based on Thematic framework
 *-- ----------------------------------------------------------*/


/**
 * ADMIN INIT
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Some theme specific variables for the options panel
$childthemename = "Meem Theme";
$childshortname = "tc";
$childoptions = array();

add_action('init','meem_theme_options');
function meem_theme_options() {
	global $childthemename, $childshortname, $childoptions;
	
	// Create array to store the Categories to be used in the drop-down select box
	$categories_obj = get_categories('hide_empty=0');
	$categories = array();
	foreach ($categories_obj as $cat) {
		$categories[$cat->cat_ID] = $cat->cat_name;
	}
	
	// the color variants array
	$color_variants = array(
						"white" => "white",
						"ahmer1" => "ahmer1",
						"ahmer2" => "ahmer2",
						"ahmer3" => "ahmer3",
						"akhzer1" => "akhzer1",
						"akhzer2" => "akhzer2",
						"akhzer3" => "akhzer3",
						"akhzer4" => "akhzer4",
						"akhzer5" => "akhzer5",
						"akhzer6" => "akhzer6",
						"asfur1" => "asfur1",
						"asfur2" => "asfur2",
						"asfur3" => "asfur3",
						"azruq1" => "azruq1",
						"azruq2" => "azruq2",
						"azruq3" => "azruq3"
						);
	// Column array
	$columns = array(
					"1" => "1",
					"2" => "2",
					"3" => "3",
					"4" => "4"
					);
	// Header radio selection
	$headers = array(
					"Title" 	=> "Title",
					"Branding"	=> "Branding"
					);
	
	// Options Array
	$childoptions = array (
		array(  "name" => __('Theme Colors','thematic'),
						//__('Choo...') used to search the language translation of this parameter if any, oterwise returns ('Choo...') 
				"desc" => __('Choose color scheme for your Webpage.','thematic'),
				"id" => "color_variant",
				"std" => 'blue',
				"type" => "select",
				"options" => $color_variants
			),
		array( "name" => __('Theme Columns','thematic'),
				"desc" => __('Arrange your Posts thumbnails in columns.', 'thematic'),
				"id" => "no_of_columns",
				"std" => '3',
				"type" => "select",
				"options" => $columns
				),
		array( "name" => __('Category','thematic'),
				"desc" => __('Select category for your post.', 'thematic'),
				"id" => "child_feature_cat",
				"std" => 'lorem',
				"type" => "select",
				"options" => $categories
				),
		array( "name" => __('Select Header Type','thematic'),
				"desc" => __('Header Consists of two parts, Title on left side and Branding on right.
								Select and Save header type here then scroll down and select Image.','thematic'),
				"id" => "header_type",
				"std" => "false",
				"type" => "radio",
				"options" => $headers
				)
		/*
		array ( "name" => __('Site Title','thematic'),
				"desc" => __('Replacing Customize Title','thematic'),
				"id" => "my_site_title",
				"std" => "Mubashir WebSite",
				"type" => "text"
				)
		
				
		array ("name" => __('Link Color', 'thematic'),
				"desc" => __('Change the color of the links by by entring HEX color number.', 'thematic'),
				"id" => "child_link_color",
				"std" => "999999",
				"type" => "text"
				),
	
			I Donot need this option ,custom header menu is already available in dashboard
			array( "name" => __('Show Header Image','thematic'),
				"desc" => __('Replace custom header image.','thematic'),
				"id" => "child_show_logo",
				"std" => "false",
				"type" => "checkbox"
				),
	*/
			);	
	}

// add menu item to wp-admin
add_action( 'admin_menu', 'meem_theme_admin_menu' );
function meem_theme_admin_menu() {
	global $childthemename, $childshortname, $childoptions;
	
	if(isset($_GET['page'])){ 
		if($_GET['page'] == basename(__FILE__)) { 
			// 'page' returns when header (see few lines down) is sent on pressing of save button
			if (isset($_REQUEST['action'])){ // I added this line to avoid error:Undefined index	
				if ( 'save' == $_REQUEST['action'] ) {
						// verifying nonce
						check_admin_referer('childtheme-save');
						// save the options
						foreach ($childoptions as $value) {
					
						/* Following 2 lines skip title_image_url option to be updated                           
						 * again in the database when the saved button on the form is pressed    
						 * It was already updated and saved in the form section below            
					     * and we donot want it to be reset to NULL                              
						*/
						if ($value['id'] == 'title_image_url'){
							continue;
							}
												
						if( isset($_REQUEST[ $value['id'] ])) {
							
							/* update or save the values entered in the option form into the database */
							update_option( $value['id'], $_REQUEST[ $value['id'] ] );
													
						} else {
							delete_option( $value['id'] );
							}
					}
					
				header("Location: themes.php?page=meem_setting_page.php&saved=true");
				/* it sends header request when save button pressed, sending page=meem_setting_page */
				die;
				
				} else if ( 'reset' == $_REQUEST['action'] ) {
					// verifying nonce
					check_admin_referer('childtheme-reset');
					// delete the options
					foreach ($childoptions as $value) {
						delete_option( $value['id'] ); }
						header("Location: themes.php?page=meem_setting_page.php&reset=true");
				die;
				}
			}
		}
	}
	/*  The code for analyzing the form input of Image gallary selection
	*/
	if (isset($_REQUEST['file'])) { 
						
						check_admin_referer('meem_gallary');
						/* verifying nonce */
						
						$attachment_id=$_REQUEST['file'];
						$image_attribute=wp_get_attachment_image_src($attachment_id);
						$image_url=$image_attribute[0];
						update_option('header_image',$image_url);
				}
					
	 /* Following Line adds a option submenu of my child theme under Appearance main menu. 'meem_theme_settings_page'
	 *  is the name of file that executes when the option page is clicked
	 *  there are other commands to add submenu in different parts of main menu. see P. 63 prof..plugin..dev.
	 */ 
	  	add_theme_page($childthemename." Options", 
						"$childthemename",
						'edit_themes',
						basename(__FILE__),
						'meem_theme_settings_page');
} // end function
 
// create settings page
function meem_theme_settings_page() {

    if( ! current_user_can( 'edit_themes' ) ) {

        wp_die( __('You can\'t play with this.', 'meemtheme') );

    }
global $childthemename, $childshortname, $childoptions;

// Saved or Updated message
if (isset($_REQUEST['saved'])) echo '<div id="message" class="updated fade"><p><strong>'.$childthemename.' settings saved.</strong></p></div>';
if (isset($_REQUEST['reset'])) echo '<div id="message" class="updated fade"><p><strong>'.$childthemename.' settings reset.</strong></p></div>';

	?>
   	<div class="wrap">
	
		<div id="icon-upload" class="icon32"></div>
		<h2><?php echo $childthemename; ?> Options<span class="version"> Ver <?php global $theme_version; echo $theme_version; ?></span></h2>
		
		<div id="poststuff">
		
		<div class="meta-box-sortables ui-sortable">
						
			<div class="postbox">
			
				<h3><span>Introduction</span></h3>
				<div class="inside">
					
					<p>This theme will give your posts a thumbnail presentation, arranges them in columns while first post stands alone. You have an option to arrange the posts into 1 to 4 columns page.</p>
					<p>You can select upto 16 specialy designed color themes whose preview is shown in form of a color strip on the setting page. There is also an option to filter posts on category basis.</p>
					<p> Header is divided into two parts, one showing the title in form of text( separate option added) or image, whereas other part known as <code>branding</code> showing the mission statement or image. </p>
					<p>Previously It was hard to include the SVG image in the custom header resulting into 'cropping error'. Therefore a new UI has been added on the setting page that adds SVG images from <strong> Media library</strong>. a plugin from benbodhi is used to incorporate SVG image support </p>
					
				</div> <!-- .inside -->
			
			</div> <!-- .postbox -->
			
		</div> <!-- .meta-box-sortables .ui-sortable -->
		
		<div id="post-body" class="metabox-holder columns-2">

			<!-- main content -->
			<div id="post-body-content">

				<div class="meta-box-sortables ui-sortable">

					<div class="postbox">

						<h3><span>Settings</span></h3>
						<div class="inside">
		
		<form method="post">
		
			<?php wp_nonce_field('childtheme-save');
			// creating nonce for this form?>
			
			<table class="form-table">
			
			<?php foreach ($childoptions as $value) {
				// Output the appropriate form element
				
				switch ( $value['type'] ) {
				
					case 'text':
			?>
						<tr valign="top">
							<th scope="row"><?php echo $value['name']; ?>:</th>
							<td>
								<input name="<?php echo $value['id']; ?>"
									id="<?php echo $value['id']; ?>"
									type="text"
									value="<?php echo stripslashes(get_option( $value['id'],$value['std'] )); ?>"
									> <!-- /> is not necessary as it was a requ. for xhtml not html so I will remove it from other input as well-->
								<br /><small class="description"><?php echo $value['desc']; ?></small>
								
							</td>
						</tr>
			<?php
			
					break;
					case 'select':
			?>
						<tr valign="top">
							<th scope="row"><?php echo $value['name']; ?></th>
							<td>
								<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" >
								<?php if ($value['name'] == 'Category') { ?>
									<option value="">All Categories</option>
								
								<?php } ?>
								<?php foreach ($value['options'] as $key=>$option) {
										//looping through array $option within another array $childoptions
										if ($key == get_option($value['id'], $value['std']) ) {
											//Here the value selected in option element of Select element
											//of this particular 'id' is retrieved by get_option method
											
											$selected = "selected=\"selected\""; //retains the selected option when the page reloads
										} else {
												$selected = "";
											}
								?>
								<option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $option; ?></option>
								
								<?php } ?>
						
								</select>
					
					<?php if ($value['id'] == 'color_variant') { ?>
						<div id = "parent"> 
						<!--Can also Dynamicaly append new ids col1,col2,col3 using script colorstrip.js
						 // But I have hardcoded it here. Because the colorstrip was disappeared after
						 // pressing save button or reloading the Admin page. In addition I have to 
						 // enqueue a stylesheet to make it working-------------------------------------->	
							<div id = "col1"></div>
							<div id = "col2"></div>
							<div id = "col3"></div>
						</div>	
						
					<?php } ?>
						<br /><small class="description"><?php echo $value['desc']; ?></small>
							</td>
							<?php if ($value['id'] == 'color_variant') { ?>
							<td>
							<p class="submit">
								<input name="save"   type="submit" value="Save changes" class="button-primary" />
								<input name="action" type="hidden"  value="save" />
							</p>
							</td>
					<?php } ?>
							</tr>
					
			<?php
					break;
					case 'textarea':
						$ta_options = $value['options'];
						?>
						<tr valign="top">
						<th scope="row"><?php echo $value['name']; ?>:</th>
						<td>
						<?php echo $value['desc']; ?>
						<textarea name="<?php echo $value['id']; ?>"
						id="<?php echo $value['id']; ?>"
						cols="<?php echo $ta_options['cols']; ?>"
						rows="<?php echo $ta_options['rows']; ?>"><?php
						echo stripslashes(get_option($value['id'], $value['std']));
					?></textarea>
						</td>
						</tr>
			<?php
					break;
					case "radio":
					?>
						<tr valign="top">
						<th scope="row"><?php echo $value['name']; ?>:</th>
						<td>
						<?php foreach ($value['options'] as $key=>$option) {
						if ($key == get_option($value['id'], $value['std']) ) {
							$checked = "checked=\"checked\"";
							} else {
							$checked = "";
							}
			?>
						<input type="radio"
							name="<?php echo $value['id']; ?>"
							value="<?php echo $key; ?>"
							<?php echo $checked; ?>
						/>
						<?php echo $option; ?>
						<br />
						<?php } ?>

						<small class="description"><?php echo $value['desc']; ?></small>
						</td>
						</tr>
						<?php
				break;
					case "checkbox":
			?>
						<tr valign="top">
						<th scope="row"><?php echo $value['name']; ?></th>
						<td>
			<?php
						if(get_option($value['id'])){
							$checked = "checked=\"checked\"";
						} else {
						$checked = "";
						}
			?>
						<input type="checkbox"
							name="<?php echo $value['id']; ?>"
							id="<?php echo $value['id']; ?>"
							value="true"
							<?php echo $checked; ?>
						/>
						<small class="description"><?php echo $value['desc']; ?></small>
						</td>
						</tr>
			<?php
				//break;
					//case 'file':
						
				break;
				default:
				break;
			} // end Switch
		}// end foreach
		
		?>
	</table>

		<p class="submit">
			<input name="save"   type="submit" value="Save changes" class="button-primary" />
			<input name="action" type="hidden"  value="save" />
		</p>
	</form>
	</div> <!-- .inside -->

					</div> <!-- .postbox -->
			
	
<!--  Separate Section for openeing Image library to select svg Files
*	 Remember that svg file when selected from customize builtin menu of thematic giving me cropping error
*	 and I have already loaded Benbodhi's plugin . Therefore this was the way out that I utilize here
*	 to have my svg image availble in the header section of my page.
*	 Thanx to Shibashake's web site for providing this idea.
--->
	<div class="postbox">
		<h3><span>Select here your Header's image</span></h3> </br>
			<div class="inside">
			
			<form  method="post">
						<tr valign="top">
						<!--<th scope="row"><?php// echo $talue['name']; ?>:</th> -->
						<th scope="row">Select the Image</th>
						
						<!-- My image gallery function on my option page -->    
			<?php
						$modal_update_href = esc_url( add_query_arg( array(
							'page' => 'meem_setting_page.php',
							//'step' => 2,
							//'saved' => 'true',
							'_wpnonce' => wp_create_nonce('meem_gallary'), // creating nonce
						), admin_url('themes.php') ) );
						
			?>
     					<td>
							<button id="choose-from-library-link" href="#" class="media_btn button-primary"
								data-update-link="<?php echo esc_attr( $modal_update_href ); ?>"
								data-choose="<?php esc_attr_e( 'Choose Title Image' ); ?>"
								data-update="<?php esc_attr_e( 'Save Selection' ); ?>">
								<?php _e( 'Go to MediaLibrary' ); ?>
							</button>
							<small class="description">Maximum Image size: Height=90px and Width=300px approx.</small>
						</td>
						</tr> 
			<?php 	//} 			//end foreach	?>		
		</form>
		
		</div> <!-- .inside-->
	</div> <!-- .postbox-->

<!-- Reset form --> 	
<form method="post">
	<?php wp_nonce_field('childtheme-reset');
		// creating nonce for this form?>
		<p class="submit">
		<input name="reset"  type="submit"  value="Reset" />
		<input name="action" type="hidden"  value="reset" />
		</p>
</form>
	
	</div> <!--#post-stuff-->
	
		</div> <!--.wrap-->
<?php
}
?>
