<?php
/**
 * Custom Child Theme Functions
 *
 * This file , previously part of 'function.php', contains basic site styling and layout, added specially for child theme plus
 * some widgets and option used for testing.
 *
 * In addition, separate php files exist for different options: multicolumn.php,meem_setting_page.php
 * as well as a separate file enqueue.php to load styles sheets and js script files using wp_enqueue method.
 *
 * This file's parent directory can be moved to the wp-content/themes directory 
 * to allow this Child theme to be activated in the Appearance - Themes section of the WP-Admin.
 *
 * Included is a basic theme setup that will add support for custom header images and custom 
 * backgrounds. There are also a set of commented theme supports that can be uncommented if you need
 * them for backwards compatibility. If you are starting a new theme, these legacy functionality can be deleted.  
 *
 * More ideas can be found in the community documentation for Thematic
 * @link http://docs.thematictheme.com
 *
 * @package ThematicSampleChildTheme
 * @subpackage ThemeInit
 */
 
 /*   Define theme setup      */
add_action('thematic_child_init', 'childtheme_setup');
function childtheme_setup() {
	/*
	 * Add support for custom background
	 * 
	 * Allow users to specify a custom background image or color.
	 * Requires at least WordPress 3.4
	 * 
	 * @link http://codex.wordpress.org/Custom_Backgrounds Custom Backgrounds
	 */
	 add_theme_support( 'custom-background' );
	 /** mmm
	 * Since I am building my own header widget and adminarea consisting of two parts ,title and mission statement
	 * I may therefore comment above line out.
	 */
	
	/**
	 * Add support for custom headers
	 * 
	 * Customize to match your child theme layout and style.
	 * Requires at least WordPress 3.4
	 * 
	 * @link http://codex.wordpress.org/Custom_Headers Custom Headers
	 *
	 */
	/*
	add_theme_support( 'custom-header', array(
		// Header image default
		'default-image' => '',
		// Header text display default
		'header-text' => true,
		// Header text color default
		'default-text-color' => '000',
		// Header image width (in pixels)
		'width'	=> '940',
		// Header image height (in pixels)
		'height' => '80', //reducing height of original header from 235 *mmm
		// Header image random rotation default
		'random-default' => false,
		// Template header style callback
		'wp-head-callback' => 'childtheme_header_style',
		// Admin header style callback
		'admin-head-callback' => 'childtheme_admin_header_style'
		) 
	);
*/
} // end function

/* Decouple branding and Blog title using existing 
 * empty function childtheme_override_blogtitle in 
 * thematic parent file header-extension.php and also
 * using customize text title from my option plugin
 * Also changinging order, bringing blog-title before branding 
*/
 function childtheme_override_brandingopen() {
	
    /* Fetching 'Radio select header type' option values stored in meem_setting_page.php   */
	global $childoptions;
	foreach ($childoptions as $value) {
	$$value['id'] = get_option($value['id'], $value['std']);
		  if($value['id'] == 'header_type') {
					$toso=($$value['id']); // $$ is important here!!
					//var_dump($toto);
					if($toso == 'Title'){
					
					/* Fetching image option stored in media library section 'form' on my setting page
					*/
								$toto = get_option('header_image');
	
								/* Storing again in mysql, previously I used variables but that didnot work
								*  as the variables are destroyed when new form submits 
								*/
								update_option('title_image',$toto); 
								//var_dump($toto);
					} 
					else { 
								$toko = get_option('header_image');
								
								/* Storing again in mysql, previously I used variables but that didnot work
								*  as the variables are destroyed when new form submits 
								*/
								update_option('brand_image',$toko); 
							}
			}
		}
	?>
	   <div id="blog-title">
			<img class = "attachment-thumb wp-post-image" src="<?php echo stripslashes(get_option('title_image')); ?>" />
	   </div> <!-- blog-title close -->
      
		 <!-- Adding Branding --> 
		 <div id="branding">
		 <!-- Adding img element in branding to try svg image -->
			<img class = "attachment-thumb wp-post-image" src="<?php echo stripslashes(get_option('brand_image')); ?>" />
		 </div> <!--Branding close . decoupling from blogtitle-->
		
	<?php 
			
	}// end function
		function childtheme_override_blogtitle() { // Dummy function to override the blogtitlt function
		}
		function childtheme_override_brandingclose() { // Dummy function to override the next branding close DIV
		}



/* = MMM-Social Media Sharing Buttons in the Post Page
-----------------------------------------------------*/

function mmm_share_socialmedia($content) {
	if(is_single()) {
		$content .= '<div class="linklife">
		If you love this post? Tell everyone you know, click any button to your RIGHT--->
		<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54f69ed11ee6c7e6" async="async"></script>
	</div>';
		}
	return $content;
}

add_filter('thematic_post','mmm_share_socialmedia',90);


/* = MMM- Author Bio shown in the Post page

function mmm_showbio($content) {
if (is_single()) {
$content .= '<div id="authorbio">';
$content .= '<h3>About ' . get_the_author() . '</h3>';
$content .= '<p>' . get_avatar(get_the_author_meta("user_email"), "50");
$content .= get_the_author_description() .'</p></div>';
}
return $content;
}

add_filter('thematic_post','mmm_showbio', '70');
 *---------------------------------------------------------*/



/** Removing unused Widgets from admin bar
*/
// remove index insert aside. mmm
	function wicked_remove_index_insert() {
		unregister_sidebar('index-insert');
	}
	add_action('init', 'wicked_remove_index_insert', 20);
	
// remove 3rd subsidiary aside. mmm
	function wicked_remove_3rd_subs_aside() {
		unregister_sidebar('3rd-subsidiary-aside');
	}
	add_action('init', 'wicked_remove_3rd_subs_aside', 20);

	
/* New Custom Widget... Author data on single page
class Author_Data_Widget extends WP_Widget {
		function Author_Data_Widget() {
			$widget_ops = array(
				'description' => 'A new widget that displays author info on single post'
				);
				$this->WP_Widget('Mubashir_data', 'Mubashir Bio Data', $widget_ops);
		}
		function form($instance) {
			$title = esc_attr($instance['title']);
			?>
			 <p>
				<label for="<?php echo $this->get_field_id('title'); ?>">Title:
				<input class="widefat"
						id="<?php echo $this->get_field_id('title'); ?>"
						name="<?php echo $this->get_field_name('title'); ?>"
						type="text"
						value="<?php echo esc_attr($title); ?>" />
				</label>
			</p>
			<?php	
		}
		function update($new_instance, $old_instance) {
			$instance = $old_instance;
			$instance['title'] = strip_tags($new_instance['title']);
			return $instance;
		}
		function widget($args, $instance) {
			extract($args, EXTR_SKIP);
			if (is_single()) {
				echo $before_widget;
				$title = apply_filters('widget_title', $instance['title']);
				if (!empty($title)) { echo $before_title . $title . $after_title; };
				echo '<div class="author-data">';
				echo get_avatar(get_the_author_meta('user_email'), 150);
				echo '<h4>' . the_author_meta('display_name') . '</h4>';
				// Is there an author description?
				if (get_the_author_meta('description')) {
				echo '<div class="description"><p>'
				. get_the_author_meta('description')
				. '</p></div>';
				}
				echo '</div>';
				echo $after_widget;
			}
		}
	}
register_widget('Author_Data_Widget');		
*/
/***** Short Codes****/
function pull_quote_sc($atts, $content = null) {
	extract(shortcode_atts(array(
							'width' => '600',
							'author' => '',
							), $atts));
	if (!$author == '') {
		$authorname = '<cite><em>' . esc_attr($author) . '</em></cite>';
	} else {
		$authorname = null;
	}
return '<blockquote class="pull-quote" style="width=' . esc_attr($width) . '">
	<p>' . $content . '</p>' . $authorname . '</blockquote>';
}
add_shortcode("pullquote", "pull_quote_sc");



?>
