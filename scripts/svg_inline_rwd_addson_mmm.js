window.addEventListener("load", function() {
jQuery(document).ready(function($){
	jQuery( cssTarget ).each(function(){
            var $img = jQuery(this);
            var imgID = $img.attr('id');
            var imgClass = $img.attr('class');
            var imgURL = $img.attr('src');

            jQuery.get(imgURL, function(data) {
            // Get the SVG tag, ignore the rest
            var $svg = jQuery(data).find('svg');
	
		    $svg=$svg.attr("viewBox","20 -8 122 122");
		$svg=$svg.attr("preserveAspectRatio","none");
		$svg=$svg.removeAttr("width");
		$svg=$svg.removeAttr("height");
		}, 'xml');
	});
});
});