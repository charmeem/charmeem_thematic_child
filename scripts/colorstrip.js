/* Jquery Script that creates a dynamic colorstrip in Admin panel
 * when a color theme is selected.
 * 
 * This gives user quick preview of the site theme without
 * reloading the page.
 *
 * I can later give it shape of JQuery plugin fuction see page 116 Silver
 *-------------------------------------------------------------------------*/
window.addEventListener("load", function() {
	jQuery(document).ready(function(){
	/*
	jQuery("option").css("background", "#f60");
	jQuery("option").hover(function() {
	//jQuery(this link).remove();
					jQuery(this).append('<link href="../wp-content/themes/thematic_child/skins/akhzer2/colorstrip.css" rel="stylesheet"/>');
				},
				function(){
				jQuery(this).append('<link href="../wp-content/themes/thematic_child/skins/ahmer1/colorstrip.css" rel="stylesheet"/>');
				});
	*/
				//slector #color_variant is dropdown option select box used for color theme selection
				jQuery("#color_variant").change(function(){
				//removing previously appended elements if any
				//jQuery("#col1,#col2, #col3, #col4,#parent link").remove();
				jQuery('#parent link').remove();
				
				//Adding html elements to be used for styling color strips in colorstrip.css
				// Not Using it for the time being : instead using hard coded in meem_setting_page as
				// wp_enqueue_style needs that to retain colorstrip during page reload or saved.. 
				// But even that is not working fully in Chrome....
				//jQuery("#parent").append('<div id="col1"></div><div id="col2"></div><div id="col3"></div><div id="col4"></div>');
				
				// copying the selected option into a variable
				var selected = jQuery(this).val();
				
				//First tried with AJAX but not giving desired result rather printing css file on screen
				//jQuery('#parent').load('../wp-content/themes/charmeem_thematic_child/skins/' + selected + '/colorstrip.css');
				
				//Dynamicaly loading css stylesheet using append and the selected variable. Seperate colorstrip file in respective option folder
				// .. is used to move levelup from default directory wp-admin
				//jQuery('#parent').hide();
				jQuery('#parent').append('<link href="../wp-content/themes/charmeem_thematic_child/skins/' + selected + '/colorstrip.css" rel="stylesheet"/>');
				//Used in testing phases
				//jQuery("#parent").css({"float":"right","display":"inline","display":"inline-block"});
				//jQuery("#parent div").css({"height":"40px","width":"40px","float":"right"});
				//jQuery("#col1").css({"background-color":"#cae0c0","margin":"-30px 420px 0 0"});
				//jQuery("#col2").css({"background-color":"#e0c0ca","margin-top":"-30px"});
				//jQuery("#col3").css({"background-color":"#bfaba0","margin-top":"-30px"});
					});
		});
});	
