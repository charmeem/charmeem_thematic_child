
<?php
/*
Theme Name: 	Meem Theme
Theme URI:		http://wordpress.org/....
Description: 	My first super dooper theme including lots of useful features. Based on Thematic Framework
Version: 		2.2
Author: 		Mubashir Mufti
Author URI: 	http://....com
License: 		GPL2
License URI:	http://www.gnu.org/licenses/gpl-2.0.html

	Copyright 2015  Mubashir Mufti  (email : mmufti@hotmail.com)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License, version 2,
	as published by the Free Software Foundation.

	This program is distributed WITHOUT ANY WARRANTY.
	See the	GNU General Public License for more details.

	The license for this software can likely be found here:
	http://www.gnu.org/licenses/gpl-2.0.html
	If not, write to the Free Software Foundation Inc.
	51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

/********************************************
 * GLOBAL VARIABLES
********************************************/
$theme_version = '3.1';											// for use on admin pages
define( 'MEEM_THEME_PATH', get_stylesheet_directory());	// define the absolute theme path for includes

/********************************************
* INCLUDES - keeping it modular
********************************************/
/* include_once is better then include because it avoids circular function calls  *
*  see p104 R. Nixon. More prefarable way is to use 'require_once'                */

include_once( MEEM_THEME_PATH . '/admin/meem_setting_page.php'); // This file sets up Theme settings page
include_once( MEEM_THEME_PATH . '/admin/theme_setup_page.php'); //some additional custom settings. old function.php
include_once(MEEM_THEME_PATH . '/library/enqueue.php'); // enqueue js & css for inline replacement & admin
include_once(MEEM_THEME_PATH . '/library/multicolumn.php'); // Theme's multicolumn feature

