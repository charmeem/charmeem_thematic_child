<?php 
/**
 * LOADS CSS AND JS FILES USING ENQUEUE METHOD 
 */
/**
 * ENQUEUE SCRIPTS AND STYLES
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Enqueue JavaScript
/*
add_action('init', 'my_svg_addson');
function my_svg_addson() {
wp_enqueue_script('mmm_svg_rwd',
					get_bloginfo('stylesheet_directory') . '/scripts/svg_inline_rwd_addson_mmm.js',
					'',
					'',
					'all'
					);
	
}
*/
//add_action('admin_head', 'my_jquery_scripts');
add_action('admin_enqueue_scripts', 'my_jquery_scripts');
function my_jquery_scripts() {

	//using WP built-in jquery function 
	wp_enqueue_script('jquery');   
	// Final Working Version of JQ script to produce dynamic colorstrip based on selected color theme
	wp_enqueue_script('mmm_colorstrip',
					get_bloginfo('stylesheet_directory') . '/scripts/colorstrip.js',
					'',
					'',
					'all'
					);
	/*wp_enqueue_script('mmm_svg_rwd',
					get_bloginfo('stylesheet_directory') . '/scripts/svg_inline_rwd_addson_mmm.js',
					'',
					'',
					'all'
					);
	*/
	/*wp_enqueue_style('mmm-svg', 
					get_bloginfo('stylesheet_directory') . '/library/svg_style.css',
					'', 
					'',
					'all'
	                 );
	*/
	
}

 
add_action('wp_print_styles', 'my_theme_styles'); // obselete as per documents
//add_action('wp_enqueue_scripts', 'my_theme_styles'); // Not giving desired result, needs troubleshooting
function my_theme_styles() {

	// load the custom options
	// The value stored in database from selected option of the particular 'id' is retrieved
	// using get_option function and stored in the 'id' variable
	global $childoptions;
	foreach ($childoptions as $value) {
		$$value['id'] = get_option($value['id'], $value['std']);
						
	}

	//Theme Color variants
	wp_enqueue_style('meem-color', 
					get_bloginfo('stylesheet_directory') . '/skins/' . $color_variant . '/skin.css',
					'', 
					'',
					'all'
					);
	
	
	// Multicolumn selection
	wp_enqueue_style('meem-column', 
					get_bloginfo('stylesheet_directory') . '/columns/' . $no_of_columns . '/columns.css',
					'', 
					'',
					'all'
	                 );
}

//add_action('admin_head', 'colorstrip_style');
add_action('admin_enqueue_scripts','colorstrip_style');
function colorstrip_style() {
	
	// load the custom options
	global $childoptions;
	foreach ($childoptions as $value) {
		$$value['id'] = get_option($value['id'], $value['std']);
	}
	
	// Giving common style to all the strips. I could not incorporate into mystyle.css
	// thats why i have to add in a seperate file here
	wp_enqueue_style('colorstrip_general', 
					get_bloginfo('stylesheet_directory') . '/skins/colorstrip_general.css',
					'', 
					'',
					'all'
					);
	
	// This enqueue style is not giving desired result in Chrome as the sheet loads when the page reloads or when saved
	// but keep it there despite selecting other option using JQ..
	wp_enqueue_style('mmm-colorstrip', 
					get_bloginfo('stylesheet_directory') . '/skins/' . $color_variant . '/colorstrip.css',
					'', 
					'',
					'all'
	                 );
	// Adding style for my theme version number
	wp_enqueue_style('mmm-version', 
					get_bloginfo('stylesheet_directory') . '/version.css',
					'', 
					'',
					'all'
	                 );
}

// Support for my svg image gallery option
add_action('admin_enqueue_scripts', 'my_header_image');
function my_header_image() {
	
	// Include in admin_enqueue_scripts action hook
	wp_enqueue_media();
	wp_enqueue_script( 'custom-header' );
}



