<?php
/* Admin Option setup
 * Features:
 * Admin setup and Form layout
 * Multicolumn post thumbnail support
 * Color Theme selection
 * Categories based Post selection
 *----------------------------------------*/
/* 
function my_funct() {
if ( !is_admin() ) { // instruction to only load if it is not the admin area
   // register your script location, dependencies and version
   wp_register_script('custom_script',
                      get_bloginfo('stylesheet_directory') . '/js/jq_test.js',
                      array('jquery'),
                      '1.0' );

   // enqueue the script
   wp_enqueue_script('jq_test');
   //wp_enqueue_script('jquery');
  
}
}
add_action('admin_head', 'my_funct');
*/
// Some theme specific variables for the options panel
$childthemename = "Thematic Child";
$childshortname = "tc";
$childoptions = array();

function child_theme_options() {
	global $childthemename, $childshortname, $childoptions;
	// Create array to store the Categories to be used in the drop-down select box
	$categories_obj = get_categories('hide_empty=0');
	$categories = array();
	foreach ($categories_obj as $cat) {
	$categories[$cat->cat_ID] = $cat->cat_name;
	}
	//////// the color variants array
	$color_variants = array(
						"black" => "black",
						"ahmer1" => "ahmer1",
						"ahmer2" => "ahmer2",
						"ahmer3" => "ahmer3",
						"akhzer1" => "akhzer1",
						"akhzer2" => "akhzer2",
						"akhzer3" => "akhzer3",
						"akhzer4" => "akhzer4",
						"akhzer5" => "akhzer5",
						"akhzer6" => "akhzer6",
						"asfur1" => "asfur1",
						"asfur2" => "asfur2",
						"asfur3" => "asfur3",
						"azruq1" => "azruq1",
						"azruq2" => "azruq2",
						"azruq3" => "azruq3"
						);
	/*					
	$blue_variants = array(
						"black" => "black",
						"blue_anlg1" => "blue_anlg1",
						"blue_mono1" => "blue_mono1",
						"blue_comp1" => "blue_comp1",
						"blue_spcomp1" => "blue_spcomp1",
						"blue_spcomp2" => "blue_spcomp2"
						);
	$red_variants = array(
						"red_anlg1" => "red_anlg1",
						"red_mono1" => "red_mono1",
						"red_comp1" => "red_comp1",
						"red_spcomp1" => "red_spcomp1",
						"red_spcomp2" => "red_spcomp2"
						);
	$yellow_variants = array(
						"yellow_anlg1" => "yellow_anlg1",
						"yellow_mono1" => "yellow_mono1",
						"yellow_comp1" => "yellow_comp1",
						"yellow_spcomp1" => "yellow_spcomp1",
						"yellow_spcomp2" => "yellow_spcomp2"
						);
	*/
	// Column array
	$columns = array(
					"1" => "1",
					"2" => "2",
					"3" => "3",
					"4" => "4"
					);
	// Options
	$childoptions = array (
		array(  "name" => __('Choose Color Themes','thematic'),
				"desc" => __('Select your color scheme.','thematic'),
				"id" => "wicked_color_variant",
				"std" => 'blue',
				"type" => "select",
				"options" => $color_variants
			),
	/*				
		array(  "name" => __('Blue theme','thematic'),
				"desc" => __('Select your color scheme.','thematic'),
				"id" => "blue_color_variant",
				"std" => 'blue',
				"type" => "multi_select",
				"options" => $blue_variants
			),
		array(  "name" => __('Red theme','thematic'),
				"desc" => __('Select your color scheme.','thematic'),
				"id" => "red_color_variant",
				"std" => 'red',
				"type" => "multi_select",
				"options" => $red_variants
			),
		array(  "name" => __('Yellow theme','thematic'),
				"desc" => __('Select your color scheme.','thematic'),
				"id" => "yellow_color_variant",
				"std" => 'yellow',
				"type" => "multi_select",
				"options" => $yellow_variants
				),
	/*
		array ("name" => __('Link Color', 'thematic'),
				"desc" => __('Change the color of the links by by entring HEX color number.', 'thematic'),
				"id" => "child_link_color",
				"std" => "999999",
				"type" => "text"
				),
	*/
	/* Donot need this option ,custom header menu is already available in dashboard
		array( "name" => __('Show Header Image','thematic'),
				"desc" => __('Replace custom header image.','thematic'),
				"id" => "child_show_logo",
				"std" => "false",
				"type" => "checkbox"
				),
	*/
			array( "name" => __('Featured Category','thematic'),
				"desc" => __('A category of posts t.', 'thematic'),
				"id" => "child_feature_cat",
				"std" => $default_cat,
				"type" => "select",
				"options" => $categories
				),
			array( "name" => __('Number of Columns','thematic'),
				"desc" => __('Arrange your Posts thumbnails in columns.', 'thematic'),
				"id" => "no_of_columns",
				"std" => $default,
				"type" => "select",
				"options" => $columns
				)
			);	
	}
add_action('init','child_theme_options');

// Adding Admin Panel
function childtheme_add_admin() {
	global $childthemename, $childshortname, $childoptions;
	if ( $_GET['page'] == basename(__FILE__) ) {
		if ( 'save' == $_REQUEST['action'] ) {
			// protect against request forgery
			check_admin_referer('childtheme-save');
			// save the options
			foreach ($childoptions as $value) {
				if( isset( $_REQUEST[ $value['id'] ] ) ) {
					update_option( $value['id'], $_REQUEST[ $value['id'] ] );//storing option values in database
				} else {
					delete_option( $value['id'] );
					}
			}
			header("Location: themes.php?page=options.php&saved=true");
			die;
			} else if ( 'reset' == $_REQUEST['action'] ) {
				// protect against request forgery
				check_admin_referer('childtheme-reset');
				// delete the options
				foreach ($childoptions as $value) {
					delete_option( $value['id'] ); }
				header("Location: themes.php?page=options.php&reset=true");
				die;
				}
	}
	add_theme_page($childthemename." Options", "$childthemename Options",'edit_themes', basename(__FILE__), 'childtheme_admin');
	}
	add_action('admin_menu' , 'childtheme_add_admin');
	
// Adding Option page or Option Form
function childtheme_admin() {
	global $childthemename, $childshortname, $childoptions;
	// Saved or Updated message
	if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$childthemename.' settings saved.</strong></p></div>';
	if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$childthemename.' settings reset.</strong></p></div>';
	?><div class="wrap">
	<h2><?php echo $childthemename; ?> Options </h2>
	<!--my Jquery slidein function -->
    	<div id="color_themes" >
		<form method="post">
		<?php wp_nonce_field('childtheme-save');
		?>
				<table class="form-table">
			<?php foreach ($childoptions as $value) {
			// Output the appropriate form element
				switch ( $value['type'] ) {
					case 'text':
			?>
					<tr valign="top">
					<th scope="row"><?php echo $value['name']; ?>:</th>
					<td>
					<input name="<?php echo $value['id']; ?>"
						id="<?php echo $value['id']; ?>"
						type="text"
						value="<?php echo stripslashes(get_option( $value['id'],$value['std'] )); ?>"
					/>
					<?php echo $value['desc']; ?>
					</td>
					</tr>
			<?php
				break;
					case 'select':
			?>
					<tr valign="top">
					<?php 
					/* My jquery slidein*/
					if ($value['name'] == 'Choose Color Themes') { ?>
					<th id="open"  scope="row"><?php echo "Choose Color Theme "; ?></th>
					<?php } else { ?> 
					<th scope="row"><?php echo $value['name']; ?></th>
					<?php } ?>
					<td>
					<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" >
						<option value="">--</option>
							<?php foreach ($value['options'] as $key=>$option) {
								if ($key == get_option($value['id'], $value['std']) ) {
									$selected = "selected=\"selected\"";
								} else {
									$selected = "";
									}
					?>
						
						<option value="<?php echo $key ?>" <?php echo $selected ?>>
							<?php echo $option; ?></option>
							<?php } ?>
						
					</select>
					<!-- Selecting only color_variant option Tab-->
					<?php if ($value['id'] == 'wicked_color_variant') { ?>
					<div id = "parent">
						<!-- Removing default image. Then using javascript to insert img element before "marker"....
						<img id="image" src="" width="470" height="270"/>
						-->
						<p id="marker"></p>
					</div>	
					<?php } ?>
					<?php// echo $value['desc']; ?>
					</td>
					</tr>
					
			<?php
				break;
		case 'textarea':
		$ta_options = $value['options'];
		?>
		<tr valign="top">
		<th scope="row"><?php echo $value['name']; ?>:</th>
		<td>
		<?php echo $value['desc']; ?>
		<textarea name="<?php echo $value['id']; ?>"
		id="<?php echo $value['id']; ?>"
		cols="<?php echo $ta_options['cols']; ?>"
		rows="<?php echo $ta_options['rows']; ?>"><?php
		echo stripslashes(get_option($value['id'], $value['std']));
		?></textarea>
		</td>
		</tr>
		<?php
		break;
		case "radio":
		?>
		<tr valign="top">
		<th scope="row"><?php echo $value['name']; ?>:</th>
		<td>
		<?php foreach ($value['options'] as $key=>$option) {
			if ($key == get_option($value['id'], $value['std']) ) {
			$checked = "checked=\"checked\"";
			} else {
			$checked = "";
			}
	?>
		<input type="radio"
				name="<?php echo $value['id']; ?>"
				value="<?php echo $key; ?>"
				<?php echo $checked; ?>
		/>
		<?php echo $option; ?>
		<br />
		<?php } ?>
		<?php echo $value['desc']; ?>
		</td>
		</tr>
		<?php
		break;
		case "checkbox":
		?>
		<tr valign="top">
		<th scope="row"><?php echo $value['name']; ?></th>
		<td>
		<?php
		if(get_option($value['id'])){
			$checked = "checked=\"checked\"";
		} else {
			$checked = "";
			}
	?>
		<input type="checkbox"
				name="<?php echo $value['id']; ?>"
				id="<?php echo $value['id']; ?>"
				value="true"
				<?php echo $checked; ?>
		/>
		<?php echo $value['desc']; ?>
		</td>
		</tr>
		
		<?php
		break;
		default:
		break;
		}
	}
	?>
	</table>
	<p class="submit">
	<input name="save" type="submit" value="Save changes" class="button-primary" />
	<input type="hidden" name="action" value="save" />
	</p>
	</form>
	<!----></div>
	</div>
	<form method="post">
	<?php wp_nonce_field('childtheme-reset'); ?>
	<p class="submit">
	<input name="reset" type="submit" value="Reset" />
	<input type="hidden" name="action" value="reset" />
	</p>
	</form>
	
	<p><?php _e('For more information … ', 'thematic'); ?></p>
	<?php
	} // end function

/*      = MMM- Adding Thumbnails in Post pages
 *      - Multicolumn support via Custom Option panel
 *		- Upto 4 Columns support ( RWD design via CSS)
 *		- Catagories based post thumbnails
 *----------------------------------------------------*/

/* add_theme_support('post-thumbnails');
 *  This functionality is not needed as Thematic has included it by Default. 
 *--------------------------------------------------------------------------*/

 set_post_thumbnail_size(240, 100, true); 
// setting default post size
add_image_size('homepage-thumbnail', 200, 100, true); 
//setting custom post thumbnail to be used in my First post
// Reserved Names: thumb, thumbnail, medium, large, post-thumbnail

// Can make new function in future..
function wicked_indexloop() {
	/*query_posts("posts_per_page=4"); */
	// Displays 4 posts of ALL CATEGORIES in certain order.
	// this function is not recommended as per codex instead<< pre_get_posts >>should be used
	// load custom options:
	global $childoptions;
	foreach ($childoptions as $value) {
	$$value['id'] = get_option($value['id'], $value['std']);
	}
// display posts based on categories selected by our child theme option panel
	/*
function mycategory( $query ) {
		if ( $query->is_main_query() ) {
            $query->set( 'cat' , $child_feature_cat );
			}
    }
add_action( 'pre_get_posts', 'mycategory' );
	*/
	query_posts("cat=" . $child_feature_cat);// Above hook is preffered but not working somehow!
	
//Posts and excerpts alongwith multicolumn support using option panel
	$counter = 1;
	$count = 0;
	if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID() ?>" <?php post_class() ?>>
		<?php 
	// To make 1st post different from the rest	
		if ($counter == 1 && !is_paged()) { 
	//	if ($counter == 1 && has_post_thumbnail() && !is_paged()) {		
			?>
			<div class = "first-post">
			<?php
			thematic_postheader();
			the_post_thumbnail('homepage-thumbnail'); // first post with custom image size
			the_excerpt();
			 ?>
			 </div>
			 <?php
			}
	// To make multi-columns of the rest of the posts...			
		if ($counter >=2) {	
			?>
			<div class="box<?php if( $count%$no_of_columns == 0 ) { echo '-1'; }; $count++;//Creating multicolumn POsts ?>">
			<?php
			thematic_postheader();
			the_post_thumbnail('thumb');     //small size for other posts other sizes are thumbnail, medium, large, post-thumbnail
			the_excerpt();?>
			 </div>
			 <?php
			}
		/*?><div class="entry-content">
		<?php */	
		//<!-- This is useless line -->  <a href="<?php the_permalink(); " class="more"> </a>
		$counter++; ?>
		<!--</div> entry-content-->
		
		</div><!-- .post -->
		<?php endwhile; 
		else: ?>
		<h2>Eek</h2>
		<p>There are no posts to show!</p>
		<?php endif;
		wp_reset_query();
	}
// Adding Read More in excerpt
	function new_excerpt_more($more) {
		global $post;
		return ' … <a class="more-link" href="'. get_permalink($post->ID) .' ">Read more &raquo;</a>';
	}
	add_filter('excerpt_more', 'new_excerpt_more');
// Changing the length of excerpt words
	function custom_excerpt_length( $length ) {
	return 35;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/* Jquery and Java Scripts
 *
 *
 *----------------------------------------------------*/ 
function jq_test(){
wp_enqueue_script('jquery');   //using WP built-in jquery function
							   // Can also use external file<script src="../js/jquery-1.7.2.min.js"> 
							   // but that is not recommended	
?>
<!--Jquery Test script
<script>	
	jQuery(document).ready(function(){
		jQuery('select').change(function(){
		jQuery(this).css("background", "yellow");
		});
	});
</script>
-->
<!--  Jquery Test script 2 
<script>
jQuery(document).ready(function() {
 jQuery('#wicked_color_variant').each(function() {
		jQuery(this).hover(
		function() {
			(#gallery img).show();
		},
		function () {
			(#gallery img).hide();
		}
	); // end hover
}); // end each
}); // end ready
</script>
-->

<!-- hide function
<script type="text/javascript">
jQuery(document).ready(function() {
	jQuery('#toto').hide().fadeIn(3000);
	});
</script>
-->

<!-- JQuery Slidein -->
<script>
jQuery(document).ready(function() {
  jQuery('#open').toggle(
      function() {
	    jQuery('#color_themes #wicked_color_variant ').slideDown(300);
		jQuery('#parent').slideDown(300);
	   jQuery(this).addClass('close');
	  },
	  function() {
		 jQuery('#color_themes #wicked_color_variant').fadeOut(400);
		 jQuery('#parent').fadeOut(400);
	     //jQuery(this).removeClass('close');
      }
  ); // end toggle
}); // end ready
</script>



<script>
window.addEventListener("load", function() {
	var changeTheme = function () { 
			//** Working test line:
			//document.getElementById("image").src = "<?php echo get_bloginfo('stylesheet_directory') . '/images/blue_mono1.jpg'?>"
			//** Working but the working directory is wp-admin which is not desired.:
			//document.getElementById("image").src = this.options[this.selectedIndex].value + ".jpg"
			//** trying creating new element
			var imageTheme = this.options[this.selectedIndex].value + ".jpg";
			var createImage = document.createElement("IMG");
			createImage.src="<?php echo get_bloginfo('stylesheet_directory') . '/images/'?>" + imageTheme;
			createImage.width="500";  //can use CHAINING with Jquery which is more efficient
			createImage.height="300";
			createImage.id="image";
			//** Better solution then 'insertbefore' as the new selected images does not pile up
			var parent = document.getElementById("parent");
			parent.replaceChild(createImage,parent.childNodes[0]);
			//parent.insertBefore(createImage,marker);
			//** Working as desired- fetching images from /image/ directory
			//* needs few modification: hiding default image, trying mouse hover instead of change
			//var imageTheme = this.options[this.selectedIndex].value + ".jpg";
			//document.getElementById("image").src = "<?php echo get_bloginfo('stylesheet_directory') . '/images/'?>" + imageTheme;
			
		}
    var changeBack = function () {
		var parent = getElementById("parent");
		var child = getElementById("image");
		parent.removeChild(child);
		//document.getElementById("image").src = "";
			}
		
	var colorVariant = document.getElementById('wicked_color_variant');
	colorVariant.addEventListener('change', changeTheme, false);
	//colorVariant.addEventListener('change', changeBack, false);
	//** creating mouse hover event
	//*option 1
	//colorVariant.addEventListener('mouseenter', changeTheme, false);
	//colorVariant.addEventListener('mouseleave', changeBack, false);
	//* option 2
	//colorVariant.addEventListener('mouseover', changeTheme, false);
	//colorVariant.addEventListener('mouseout', changeBack, false);
	
});
</script>
<?php 
}
//add_action('wp_enqueue_scripts', 'jq_test'); // not working as expected
add_action('admin_head', 'jq_test');
//add_action('wp_print_styles', 'jq_test');
//add_action('admin_menu', 'jq_test');
//add_action('admin_enqueue_scripts', 'jq_test');
//add_action('admin_init', 'jq_test');
?>

