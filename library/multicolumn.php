<?php
/*      Version:2.0   Date:12-07-15  Features: Code cleanup
/*      = MMM- Adding Thumbnails in Post pages
 *      - Multicolumn support via Custom Option panel
 *		- Upto 4 Columns support ( RWD design via CSS)
 *		- Catagories based post thumbnails
 *----------------------------------------------------*/

/* add_theme_support('post-thumbnails');
 *  This functionality is not needed as Thematic has included it by Default. 
 *--------------------------------------------------------------------------*/

 set_post_thumbnail_size(240, 100, true); 
// setting default post size
add_image_size('homepage-thumbnail', 200, 100, true); 
//setting custom post thumbnail to be used in my First post
// Reserved Names: thumb, thumbnail, medium, large, post-thumbnail

function multicolumn_indexloop() {
	/*query_posts("posts_per_page=4"); */
	// Displays 4 posts of ALL CATEGORIES in certain order.
	// this function is not recommended as per codex instead<< pre_get_posts >>should be used
	// load custom options:
	global $childoptions;
	foreach ($childoptions as $value) {
	$$value['id'] = get_option($value['id'], $value['std']);
	}
// display posts based on categories selected by our child theme option panel
	/*
function mycategory( $query ) {
		if ( $query->is_main_query() ) {
            $query->set( 'cat' , $child_feature_cat );
			}
    }
add_action( 'pre_get_posts', 'mycategory' );
	*/
	query_posts("cat=" . $child_feature_cat);// Above hook is preffered but not working somehow!
	
//Posts and excerpts along with multicolumn support using option panel
	$counter = 1;
	$count = 0;
	// Loop
	if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div id="post-<?php the_ID() ?>" <?php post_class() ?>>
		<?php 
	// To make 1st post different from the rest	
		if ($counter == 1 && !is_paged()) { 
	//	if ($counter == 1 && has_post_thumbnail() && !is_paged()) {		
			?>
			<div class = "first-post">
			<?php
			thematic_postheader();
			the_post_thumbnail('homepage-thumbnail'); // first post with custom image size
			the_excerpt();
			 ?>
			 </div>
			 <?php
			}
	// To make multi-columns of the rest of the posts...			
		if ($counter >=2) {	
			?>
			<div class="box<?php if( $count%$no_of_columns == 0 ) { echo '-1'; }; $count++;//Creating multicolumn POsts ?>">
			<?php
			thematic_postheader();
			the_post_thumbnail('thumb');     //small size for other posts other sizes are thumbnail, medium, large, post-thumbnail
			the_excerpt();?>
			 </div>
			 <?php
			}
		/*?><div class="entry-content">
		<?php */	
		//<!-- This is useless line -->  <a href="<?php the_permalink(); " class="more"> </a>
		$counter++; ?>
		<!--</div> entry-content-->
		
		</div><!-- .post -->
		<?php endwhile; 
		else: ?>
		<h2>Eek</h2>
		<p>There are no posts to show!</p>
		<?php endif;
		wp_reset_query();
	}
// Adding Read More in excerpt
	function new_excerpt_more($more) {
		global $post;
		return ' … <a class="more-link" href="'. get_permalink($post->ID) .' ">Read more &raquo;</a>';
	}
	add_filter('excerpt_more', 'new_excerpt_more');
// Changing the length of excerpt words
	function custom_excerpt_length( $length ) {
	return 35;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
