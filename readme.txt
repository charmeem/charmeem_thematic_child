In this theme I have added some features like color harmonics , multicolumn, header division into 2 parts, svg image support in header and RWD and many more. 
This theme is built as a child of Thematic Framework.

Regards!

--
Mubashir Mufti
mubashir@charmeem.com
http://charmeem.com/